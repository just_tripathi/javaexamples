package com.db.mocking;

/**
 * @author Parag
 * This is someones else contract which i have used till there is no implementatiion
 * does it mean it is stopping from the testing of my application ...... Yes/No
 * <p>
 * has parag implemented the logic the answer is no...
 */
public interface IBankService {
    String transaction(TransactionType type, String accountid, double amount);
}

