package com.db.airportsapp;

import java.util.List;

public interface IAirportsApp {
    List<Airport> findAirportByCode(String code);

    List<Airport> findAirportByName(String name);

    List<Airport> findAirportByLatitude(String Latitude);

    List<Airport> findAirportByLongitude(String Longitude);

    List<Airport> findAirportByAddress(String Address);

}
