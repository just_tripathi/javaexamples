package com.db.lambdafunctions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class AirportsFunctionDemo {
    public static void main(String[] args) throws IOException {


        List<String[]> helipads = Files.readAllLines(Paths.get("C:\\Users\\user\\Downloads\\airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findHeliPorts).
                collect(Collectors.toList());
        List<String[]> smap = Files.readAllLines(Paths.get("C:\\Users\\user\\Downloads\\airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findSmallAirports).
                collect(Collectors.toList());

        List<String[]> lgap = Files.readAllLines(Paths.get("C:\\Users\\user\\Downloads\\airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findLargeAirports).
                collect(Collectors.toList());

        List<String[]> medap = Files.readAllLines(Paths.get("C:\\Users\\user\\Downloads\\airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findMediumAirport).
                collect(Collectors.toList());

        System.out.println(helipads.size());
        System.out.println(smap.size());
        System.out.println(lgap.size());
        System.out.println(medap.size());
    }
        public static String[] transformByComma(String data) {
            return data.split(",");
        }

        public static boolean findHeliPorts(String[] data) {
            return data[2].equals("\"heliport\"");
        }

        public static boolean findLargeAirports(String[] data) {
            return data[2].equals("\"large_airport\"");
        }

        public static boolean findSmallAirports(String[] data) {
            return data[2].equals("\"small_airport\"");
        }

        public static boolean findMediumAirport(String[] data) {
            return data[2].equals("\"medium_airport\"");
        }

        public static boolean findClosedAirports(String[] data) {
            return data[2].equals("\"closed\"");
        }


        public static boolean findBallonPort(String[] data) {
            return data[2].equals("\"balloonport\"");
        }



}
