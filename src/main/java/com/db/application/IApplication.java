package com.db.application;

public interface IApplication {
    /**
     * returns our version application
     * @return String
     */
    public String getVersion();

    /**
     Deposit money to bank

     */
    double deposit(String accountId, double amount);
    double withdraw(String accountId, double amount);
}
