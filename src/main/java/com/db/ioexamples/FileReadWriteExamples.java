package com.db.ioexamples;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReadWriteExamples {
    public static void main(String[] args) {
        long startTime = System.currentTimeMillis();
        // readFile();
        //readFileUsingReader();
        readFilesNativeWithStreams();
        long endTime = System.currentTimeMillis();
        System.out.println(endTime-startTime);
    }

    /**java.io.InputStream is;
    java.io.OutputStream os;

    java.io.Reader reader;
    java.io.Writer writer;

    java.io.File file;
    java.nio.file.Files nFiles;*/

        public static void readFile() {
            try {
                InputStream is = new FileInputStream("\"C:\\Users\\user\\Downloads\\airports.csv\"");
                byte[] buffer = new byte[is.available()];
                is.read(buffer);
                System.out.println(new String(buffer));
            } catch (Exception e) {
                System.out.println(e);
            }
        }


            public static void readFileUsingReader() {
                // a varaible to store each line
                String data = "";
                // the buffered reader
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(new FileReader("C:\\Users\\user\\Downloads\\airports.csv"));
                    while ((data = reader.readLine()) != null) {
                        System.out.println(data);
                    }
                } catch (Exception fe) {
                    System.out.println(fe);
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

    public static void readFilesNativeWithStreams() {
        try {
            System.out.println(Files.readString(Paths.get("C:\\Users\\user\\Downloads\\airports.csv")));
        } catch (Exception exception) {
            // exception curation
        } finally {
            // Close your resource if na open
        }

    }



}
